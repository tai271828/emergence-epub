#!/usr/bin/env bash
target_epub=build/epub/book.epub
rm -f ${target_epub}
make epub
ebook-viewer ${target_epub} &
